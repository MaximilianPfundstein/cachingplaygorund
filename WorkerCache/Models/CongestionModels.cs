namespace WorkerCache.Models;

public class CongestionModels
{
    public class CongestionInformationResponse
    {
        public DateTime Day { get; set; }

        public DateTime UpdatedUtc { get; set; }

        public IList<PeriodResponse> CongestionPeriods { get; set; } =
            new List<PeriodResponse>();
    }

    public class PeriodResponse
    {
        public DateTime StartUtc { get; set; }

        public DateTime EndUtc { get; set; }
    }
}