namespace WorkerCache;

public interface IStrategy<in TObj, TItem>
{
    IEnumerable<(TItem Item, string Key)> Pick(TObj? obj);
}

public abstract class Strategy<TObj, TItem> : IStrategy<TObj, TItem>
{
    public IEnumerable<(TItem Item, string Key)> Pick(TObj? obj)
    {
        if (obj == null)
            return new List<(TItem Item, string Key)>();

        var items = SelectItems(obj)
            .Select(item => (Item: item, Key: KeyPostfix(item)))
            .ToList();

        var hasUniqueKeys = items.Select(item => item.Key).Distinct().Count() == items.Select(item => item.Key).Count();

        if (!hasUniqueKeys)
            throw new ArgumentException("Strategy selector returned ambiguous keys");

        return items;
    }

    protected abstract string KeyPostfix(TItem item);

    protected abstract IEnumerable<TItem> SelectItems(TObj obj);
}

public class DefaultStrategy<TObj> : Strategy<TObj, TObj>
{
    protected override string KeyPostfix(TObj item)
    {
        return typeof(TObj).DataKey();
    }

    protected override IEnumerable<TObj> SelectItems(TObj obj)
    {
        return new List<TObj>
        {
            obj
        };
    }
}