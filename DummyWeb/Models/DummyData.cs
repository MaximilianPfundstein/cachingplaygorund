namespace DummyWeb.Models;

public class DummyData
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public DateTime Date { get; set; }
}

public class DummyDataWrapper
{
    public IList<DummyData> Dataaaaas { get; set; } = new List<DummyData>();
}