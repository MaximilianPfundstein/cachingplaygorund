using System.Text.Json;

namespace WorkerCache;

public interface ISerializer
{
    public byte[] Serialize<T>(T obj);
    public T Deserialize<T>(byte[] byteArray);
}

public class DefaultSerializer : ISerializer
{
    public byte[] Serialize<T>(T obj)
    {
        return JsonSerializer.SerializeToUtf8Bytes(obj);
    }

    public T Deserialize<T>(byte[] byteArray)
    {
        return JsonSerializer.Deserialize<T>(byteArray) ?? throw new ArgumentNullException(nameof(byteArray));
    }
}