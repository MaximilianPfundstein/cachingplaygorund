using DummyWeb.Models;
using Microsoft.Extensions.Caching.Distributed;
using WorkerCache;

namespace DummyWeb.CacheWorkers;

public class DummyDataCacheUpdater : AbstractDistributedCacheWorker<DummyDataWrapper>
{
    public DummyDataCacheUpdater(IDistributedCache distributedCache, ISerializer serializer,
        IStrategy<DummyDataWrapper, DummyDataWrapper> strategy) : base(distributedCache, serializer, strategy)
    {
    }

    protected override TimeSpan SleepInterval { get; } = TimeSpan.FromSeconds(20);

    protected override async Task<DummyDataWrapper> GetData(CancellationToken cancellationToken)
    {
        await Task.Delay(3000, cancellationToken);

        var dataWrapper = new DummyDataWrapper
        {
            Dataaaaas = new List<DummyData>
            {
                new()
                {
                    Id = 1,
                    Name = "Hans",
                    Date = DateTime.UtcNow
                },
                new()
                {
                    Id = 2,
                    Name = "Mayer",
                    Date = DateTime.UtcNow
                },
                new()
                {
                    Id = 3,
                    Name = "Herbert",
                    Date = DateTime.UtcNow
                }
            }
        };

        return await Task.FromResult(dataWrapper);
    }
}