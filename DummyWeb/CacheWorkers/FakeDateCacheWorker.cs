using Microsoft.Extensions.Caching.Distributed;
using WorkerCache;
using WorkerCache.Models;

namespace DummyWeb.CacheWorkers;

public class FakeDateCacheUpdater : AbstractDistributedCacheWorker<IList<CongestionModels.CongestionInformationResponse>,
    CongestionModels.CongestionInformationResponse>
{
    public FakeDateCacheUpdater(IDistributedCache distributedCache, ISerializer serializer,
        IStrategy<IList<CongestionModels.CongestionInformationResponse>, CongestionModels.CongestionInformationResponse>
            strategy) : base(distributedCache,
        serializer, strategy)
    {
    }

    protected override TimeSpan SleepInterval { get; } = TimeSpan.FromSeconds(20);

    protected override async Task<IList<CongestionModels.CongestionInformationResponse>> GetData(
        CancellationToken cancellationToken)
    {
        await Task.Delay(3000, cancellationToken);
        return await Task.FromResult(new List<CongestionModels.CongestionInformationResponse>
        {
            new()
            {
                Day = DateTime.UtcNow.Date,
                UpdatedUtc = DateTime.UtcNow,
                CongestionPeriods = new List<CongestionModels.PeriodResponse>
                {
                    new()
                    {
                        StartUtc = DateTime.UtcNow.AddHours(1),
                        EndUtc = DateTime.UtcNow.AddHours(-1)
                    },
                    new()
                    {
                        StartUtc = DateTime.UtcNow.AddHours(2),
                        EndUtc = DateTime.UtcNow.AddHours(3)
                    },
                    new()
                    {
                        StartUtc = DateTime.UtcNow.AddHours(4),
                        EndUtc = DateTime.UtcNow.AddHours(5)
                    }
                }
            },
            new()
            {
                Day = DateTime.UtcNow.AddDays(1).Date,
                UpdatedUtc = DateTime.UtcNow.AddDays(2),
                CongestionPeriods = new List<CongestionModels.PeriodResponse>
                {
                    new()
                    {
                        StartUtc = DateTime.UtcNow.AddHours(1),
                        EndUtc = DateTime.UtcNow.AddHours(-1)
                    },
                    new()
                    {
                        StartUtc = DateTime.UtcNow.AddHours(2),
                        EndUtc = DateTime.UtcNow.AddHours(3)
                    },
                    new()
                    {
                        StartUtc = DateTime.UtcNow.AddHours(4),
                        EndUtc = DateTime.UtcNow.AddHours(5)
                    }
                }
            }
        });
    }
}