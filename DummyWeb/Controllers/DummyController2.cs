using System.Globalization;
using Microsoft.AspNetCore.Mvc;
using WorkerCache;
using WorkerCache.Models;

namespace DummyWeb.Controllers;

[ApiController]
[Route("[controller]")]
public class HummyController : ControllerBase
{
    private readonly DistributedCache _distributedWorkerCache;

    public HummyController(DistributedCache distributedWorkerCache)
    {
        _distributedWorkerCache = distributedWorkerCache;
    }


    [HttpGet(Name = "GetCongestionData")]
    public async Task<IActionResult> GetCongestion()
    {
        var dummyDataWrapper =
            await _distributedWorkerCache.Get<CongestionModels.CongestionInformationResponse>(
                DateTime.UtcNow.Date.ToString(CultureInfo.InvariantCulture));

        if (dummyDataWrapper == null)
            return NoContent();

        return Ok(dummyDataWrapper);
    }
}