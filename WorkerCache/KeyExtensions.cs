namespace WorkerCache;

public static class KeyExtensions
{
    private static string KeyPrefix(Type type)
    {
        return type.FullName ?? type.Name;
    }

    public static string DataKey(this Type type)
    {
        return $"{KeyPrefix(type)}:Data";
    }

    public static string LastUpdateKey(this Type type)
    {
        return $"{KeyPrefix(type)}:LastUpdate";
    }
}