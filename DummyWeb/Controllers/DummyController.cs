using DummyWeb.Models;
using Microsoft.AspNetCore.Mvc;
using WorkerCache;

namespace DummyWeb.Controllers;

[ApiController]
[Route("[controller]")]
public class DummyController : ControllerBase
{
    private readonly DistributedCache _distributedWorkerCache;

    public DummyController(DistributedCache distributedWorkerCache)
    {
        _distributedWorkerCache = distributedWorkerCache;
    }

    [HttpGet(Name = "GetDummyData")]
    public async Task<IActionResult> GetDummy()
    {
        var dummyDataWrapper = await _distributedWorkerCache.Get<DummyDataWrapper>();

        if (dummyDataWrapper == null)
            return NoContent();

        return Ok(dummyDataWrapper);
    }
}