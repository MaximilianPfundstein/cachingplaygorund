using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.StackExchangeRedis;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;

namespace WorkerCache;

public class DistributedCache
{
    private readonly ISerializer _serializer;

    public DistributedCache(IDistributedCache distributedCacheInternal, ISerializer serializer)
    {
        _serializer = serializer;
        DistributedCacheInternal = distributedCacheInternal;
    }

    private IDistributedCache DistributedCacheInternal { get; }

    public async Task<T?> Get<T>(string? keySelector = null) where T : class
    {
        var key = keySelector == null ? typeof(T).DataKey() : $"{typeof(T).DataKey()}:{keySelector}";
        var valueAsByteArray = await DistributedCacheInternal.GetAsync(key);
        return valueAsByteArray == null ? null : _serializer.Deserialize<T>(valueAsByteArray);
    }
}

public static class ServiceCollectionExtensions
{
    private static bool? _selectedInMemory;

    public static void AddDistributedWorkerCache<TWorker, TCacheObject>(
        this IServiceCollection serviceCollection, bool inMemory = false,
        string redisConnectionString = "")
        where TWorker : AbstractDistributedCacheWorker<TCacheObject, TCacheObject>, IHostedService
    {
        serviceCollection.AddDistributedWorkerCache<TWorker, TCacheObject, TCacheObject, DefaultSerializer>(inMemory,
            redisConnectionString);
    }

    public static void AddDistributedWorkerCache<TWorker, TCacheObject, TCacheItem>(
        this IServiceCollection serviceCollection, bool inMemory = false,
        string redisConnectionString = "")
        where TWorker : AbstractDistributedCacheWorker<TCacheObject, TCacheItem>, IHostedService
    {
        serviceCollection.AddDistributedWorkerCache<TWorker, TCacheObject, TCacheItem, DefaultSerializer>(inMemory,
            redisConnectionString);
    }

    public static void AddDistributedWorkerCache<TWorker, TCacheObject, TCacheItem, TSerializer>(
        this IServiceCollection serviceCollection, bool inMemory = false,
        string redisConnectionString = "")
        where TWorker : AbstractDistributedCacheWorker<TCacheObject, TCacheItem>
        where TSerializer : class, ISerializer
    {
        if (_selectedInMemory == null)
            _selectedInMemory = inMemory;
        else if (_selectedInMemory != inMemory)
            throw new ArgumentException("Distributed cache must either be fully in memory or not", nameof(inMemory));


        // Will also not add duplicate implementations of T
        serviceCollection.AddHostedService<TWorker>();
        serviceCollection.TryAddSingleton<DistributedCache>();
        serviceCollection.TryAddSingleton<ISerializer, TSerializer>();
        serviceCollection.TryAddSingleton<IStrategy<TCacheObject, TCacheObject>, DefaultStrategy<TCacheObject>>();

        if (inMemory)
            // Uses TryAdd, so not adding duplicates
            serviceCollection.AddDistributedMemoryCache();
        else
            // Register the RedisCache service
            serviceCollection.TryAddStackExchangeRedisCache(
                options => { options.Configuration = redisConnectionString; });
    }


    // Original implementation does not use try add, hence copy&paste&adjust.
    /// <summary>
    ///     Adds Redis distributed caching services to the specified <see cref="IServiceCollection" />.
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection" /> to add services to.</param>
    /// <param name="setupAction">
    ///     An <see cref="Action{RedisCacheOptions}" /> to configure the provided
    ///     <see cref="RedisCacheOptions" />.
    /// </param>
    /// <returns>The <see cref="IServiceCollection" /> so that additional calls can be chained.</returns>
    public static IServiceCollection TryAddStackExchangeRedisCache(this IServiceCollection services,
        Action<RedisCacheOptions> setupAction)
    {
        if (services == null) throw new ArgumentNullException(nameof(services));

        if (setupAction == null) throw new ArgumentNullException(nameof(setupAction));

        services.AddOptions();
        services.Configure(setupAction);
        services.TryAdd(ServiceDescriptor.Singleton<IDistributedCache, RedisCache>());

        return services;
    }
}