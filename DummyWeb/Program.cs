using DummyWeb.Builder;
using DummyWeb.CacheWorkers;
using DummyWeb.Models;
using WorkerCache;
using WorkerCache.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var redisConnectionString = builder.Configuration.GetSection("Redis")["ConnectionString"];

/*
builder.Services.AddDistributedWorkerCache<DummyDataCacheUpdater, DummyDataWrapper>(
    redisConnectionString: redisConnectionString);
builder.Services.AddDistributedWorkerCache<FakeDateCacheUpdater, IList<CongestionInformationResponse>, CongestionInformationResponse>(
    redisConnectionString: redisConnectionString);
    */

//

builder.Services
    .AddDistributedCache()
    .AsRedisCacheCache(options => options.Configuration = redisConnectionString)
    .WithDefaultSerializer()
    .WithWorkerHavingDefaultStrategy<DummyDataCacheUpdater, DummyDataWrapper>()
    .WithWorkerHavingCustomStrategy<FakeDateCacheUpdater, IList<CongestionModels.CongestionInformationResponse>,
        CongestionModels.CongestionInformationResponse>(typeof(CongestionStrategy));

//

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();