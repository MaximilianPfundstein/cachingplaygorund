using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Hosting;
using Serilog;
using StackExchange.Redis;

namespace WorkerCache;

public abstract class AbstractDistributedCacheWorker<T> : AbstractDistributedCacheWorker<T, T>
{
    protected AbstractDistributedCacheWorker(IDistributedCache distributedCache, ISerializer serializer, IStrategy<T, T> strategy) : base(distributedCache, serializer, strategy)
    {
    }
}

public abstract class AbstractDistributedCacheWorker<TGet, TSet> : BackgroundService
{
    private readonly IDistributedCache _distributedCache;
    private readonly ISerializer _serializer;
    private readonly IStrategy<TGet, TSet> _strategy;

    protected AbstractDistributedCacheWorker(IDistributedCache distributedCache, ISerializer serializer,
        IStrategy<TGet, TSet> strategy)
    {
        _distributedCache = distributedCache;
        _serializer = serializer;
        _strategy = strategy;
    }

    protected virtual TimeSpan RefreshInterval { get; } = new(0, 1, 0, 0);
    protected virtual TimeSpan SleepInterval { get; } = new(0, 0, 1, 0);
    protected abstract Task<TGet> GetData(CancellationToken cancellationToken);

    private async Task<IEnumerable<(TSet Item, string Key)>> GetStrategyData(CancellationToken cancellationToken)
    {
        var data = await GetData(cancellationToken);
        return _strategy.Pick(data);
    }

    protected override async Task ExecuteAsync(CancellationToken cancellationToken)
    {
        if (SleepInterval > RefreshInterval)
            Log.Warning("Sleep interval of a distributed cache worker should not be larger than its refresh interval");

        while (!cancellationToken.IsCancellationRequested)
        {
            try
            {
                var lastUpdateAsByteArray =
                    await _distributedCache.GetAsync(typeof(TGet).LastUpdateKey(), cancellationToken);

                if (lastUpdateAsByteArray == null)
                {
                    await Step(cancellationToken);
                }
                else
                {
                    var lastUpdate = _serializer.Deserialize<DateTime>(lastUpdateAsByteArray);

                    // If update is due
                    if (lastUpdate + RefreshInterval < DateTime.UtcNow)
                        await Step(cancellationToken);
                }
            }
            catch (RedisConnectionException rex)
            {
                Log
                    .ForContext("Type", typeof(TGet).FullName)
                    .Error(rex, "Cannot reach redis");
            }
            catch (Exception ex)
            {
                Log
                    .ForContext("Type", typeof(TGet).FullName)
                    .Fatal(ex,
                        "Encountered exception when trying to update distributed worker cache, redis is (!) reachable - will retry in a while");
            }

            await Task.Delay(SleepInterval, cancellationToken);
        }
    }

    private async Task Step(CancellationToken cancellationToken)
    {
        var data = await GetStrategyData(cancellationToken);
        var saveDataTasks = data.Select(entry =>
            _distributedCache.SetAsync(entry.Key, _serializer.Serialize(entry.Item), cancellationToken)).ToList();

        await Task.WhenAll(saveDataTasks);

        await _distributedCache.SetAsync(typeof(TSet).LastUpdateKey(), _serializer.Serialize(DateTime.UtcNow),
            cancellationToken);
    }
}