using System.Globalization;
using WorkerCache.Models;

namespace WorkerCache;

public class CongestionStrategy : Strategy<IList<CongestionModels.CongestionInformationResponse>,
    CongestionModels.CongestionInformationResponse>
{
    protected override string KeyPostfix(CongestionModels.CongestionInformationResponse item)
    {
        return item.Day.ToString(CultureInfo.InvariantCulture);
    }

    protected override IEnumerable<CongestionModels.CongestionInformationResponse> SelectItems(
        IList<CongestionModels.CongestionInformationResponse> obj)
    {
        return obj.Select(ci => ci);
    }
}