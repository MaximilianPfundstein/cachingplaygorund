using Microsoft.Extensions.Caching.StackExchangeRedis;
using Microsoft.Extensions.DependencyInjection.Extensions;
using WorkerCache;

namespace DummyWeb.Builder;

public class CacheBuilder
{
    protected readonly IServiceCollection ServiceCollection;

    protected CacheBuilder(IServiceCollection serviceCollection)
    {
        ServiceCollection = serviceCollection;
        ServiceCollection.TryAddSingleton<DistributedCache>();
    }
}

public class CacheBuilderAs : CacheBuilder
{
    public CacheBuilderAs(IServiceCollection serviceCollection) : base(serviceCollection)
    {
    }

    public CacheBuilderWithSerializer AsMemoryCache()
    {
        ServiceCollection.AddDistributedMemoryCache();
        return new CacheBuilderWithSerializer(ServiceCollection);
    }

    public CacheBuilderWithSerializer AsRedisCacheCache(Action<RedisCacheOptions> setupAction)
    {
        ServiceCollection.TryAddStackExchangeRedisCache(setupAction);
        return new CacheBuilderWithSerializer(ServiceCollection);
    }
}

public class CacheBuilderWithSerializer : CacheBuilder
{
    public CacheBuilderWithSerializer(IServiceCollection serviceCollection) : base(serviceCollection)
    {
    }

    public CacheBuilderWithWorker WithSerializer<TSerializer>() where TSerializer : class, ISerializer
    {
        ServiceCollection.TryAddSingleton<ISerializer, TSerializer>();
        return new CacheBuilderWithWorker(ServiceCollection);
    }

    public CacheBuilderWithWorker WithDefaultSerializer()
    {
        ServiceCollection.TryAddSingleton<ISerializer, DefaultSerializer>();
        return new CacheBuilderWithWorker(ServiceCollection);
    }
}

public class CacheBuilderWithWorker : CacheBuilder
{
    public CacheBuilderWithWorker(IServiceCollection serviceCollection) : base(serviceCollection)
    {
    }

    public CacheBuilderWithWorker WithWorkerHavingDefaultStrategy<TWorker, TCacheObject>()
        where TWorker : AbstractDistributedCacheWorker<TCacheObject>
    {
        ServiceCollection.AddHostedService<TWorker>();
        ServiceCollection.TryAddSingleton<IStrategy<TCacheObject, TCacheObject>, DefaultStrategy<TCacheObject>>();
        return this;
    }

    public CacheBuilderWithWorker WithWorkerHavingCustomStrategy<TWorker, TCacheObject, TCacheItem>(Type strategyType)
        where TWorker : AbstractDistributedCacheWorker<TCacheObject, TCacheItem>
    {
        ServiceCollection.AddHostedService<TWorker>();
        ServiceCollection.TryAddSingleton(typeof(IStrategy<TCacheObject, TCacheItem>), strategyType);
        return this;
    }
}

public static class BuilderExtensions
{
    public static CacheBuilderAs AddDistributedCache(this IServiceCollection serviceCollection)
    {
        return new CacheBuilderAs(serviceCollection);
    }
}